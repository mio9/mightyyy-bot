module.exports = {
  getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  },
  getOrdinal(n) {
    var s = ["th", "st", "nd", "rd"],
      v = n % 100;
    return n + (s[(v - 20) % 10] || s[v] || s[0]);
  },
  cleanTagsFormat(text) {
  if (!text) {
    return undefined;
  }
  return text.replace(/[\<\>\#\@\&]/g, "");
}
};
