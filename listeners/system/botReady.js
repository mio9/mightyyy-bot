const { Listener } = require("discord-akairo");

class BotReadyListener extends Listener {
  constructor() {
    super("botReady", {
      eventName: "ready",
      emitter: "client"
    });
  }

  exec() {
    console.log("Bot Initialization completed");
    this.client.user.setActivity("on TrainCarts Server",{type: "PLAYING"})
    this.client.generateInvite(["ADMINISTRATOR"])
      .then(link => console.log(`Generated bot invite link: ${link}`))
      .catch(console.error);
  }
}

module.exports = BotReadyListener;
