const { Listener } = require("discord-akairo");
const DiscordJS = require("discord.js");
const util = require("../util");

class MemberJoinListener extends Listener {
  constructor() {
    super("memberJoin", {
      eventName: "guildMemberAdd",
      emitter: "client"
    });
  }

  exec(member) {
    const joinInviteID = this.client.settings.get(
      member.guild.id,
      "invite_link"
    );
    if (!joinInviteID) {
      return;
    }
    const time = new Date();
    let embed = new DiscordJS.RichEmbed();
    let welcomeChannelId = this.client.settings.get(
      member.guild.id,
      "welcome_channel"
    );
    if (!welcomeChannelId) {
      return;
    }
    let welcomeChannel = this.client.util.resolveChannel(
      welcomeChannelId,
      member.guild.channels
    );
    member.guild.fetchInvites().then(invites => {
      let inviteUses = invites.find(invite => invite.code == joinInviteID).uses;
      const welcomeMessage = this.client.settings.get(
        member.guild.id,
        "welcome_message"
      );

      let processedMessage = welcomeMessage
        .replace("{user}", `<@${member.id}>`)
        .replace("{join_number}", util.getOrdinal(inviteUses))
        .replace("{member_count}", member.guild.memberCount)
        .split("\n");
      // console.log(processedMessage);
      let messageTitle = processedMessage[0];
      processedMessage.shift();
      let messageContent = processedMessage.join("\n");
      let braodcastMessage = embed
        .setDescription(messageContent)
        .setThumbnail(member.user.avatarURL)
        .setColor(util.getRandomColor())
        .setTitle(messageTitle)
        .setAuthor("TC Bot")
        .setFooter(
          `${time.getUTCDate()}/${time.getUTCMonth()}/${time.getUTCFullYear()} ${time.getUTCHours()}:${time.getUTCMinutes()}`
        );
      welcomeChannel.send(braodcastMessage);
    });
  }
}

module.exports = MemberJoinListener;
