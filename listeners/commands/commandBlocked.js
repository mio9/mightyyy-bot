const { Listener } = require("discord-akairo");

const cmdBlockReasons = new Map([
  ["owner", "You need be the bot owner before executing this command"],
  ["clientPermissions", "You do not have suffucient permission (client)"],
  ["userPermissions", "You do not have sufficient permission (user)"],
  ["guild", "This command only works on discord servers"]
]);


class CommandBlockedListener extends Listener {
  constructor() {
    super("cmdBlock", {
      emitter: "commandHandler",
      eventName: "commandBlocked"
    });
  }

  exec(message, command, reason) {
    message.reply(
      "⛔ **Unable to execute command:** " + cmdBlockReasons.get(reason)
    );
    console.log(
      `${message.author.username} was blocked from using \'${
        command.id
      }\' with reason: ${reason}!`
    );
  }
}


module.exports=CommandBlockedListener;
