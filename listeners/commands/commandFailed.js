const { Listener } = require("discord-akairo");

const cmdFailedReasons = new Map([
  ["announce_channel_not_set", "Announcement channel is not set yet"],
  ['notify_rank_not_set', 'Notification rank is not set yet']
]);

class CommandFailedListener extends Listener {
  constructor() {
    super("cmdFail", {
      emitter: "commandHandler",
      eventName: "commandFailed"
    });
  }

  exec(message, command, reason) {
    message.channel.send(
      "⛔ **Failed to execute command:** " + cmdFailedReasons.get(reason)
    );
    console.log(
      `${message.author.username} was failed from using \'${
        command.id
      }\' with reason: ${reason}!`
    );
  }
}


module.exports=CommandFailedListener;
