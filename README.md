# TC Bot

The official discord bot for service at the Traincarts Server

## Commands
Commands are prefixed with `!tc` , space between `!tc` and first argument is optional, e.g. `!tc settings` is identical with `!tcsettings`

### Functions
* Announce
  * **Main Command:**
    * `!tc announce "<content>"` - Make announcement to the announcement channel
  * **Flags:** 
    * `--title="[title]"` - Set the title of announcement. If not provided, `TrainCarts Server Announcement` will be used
    * `--notify` - Mention @Notify with announcement
    * `--color="[hexcolor]"` - Color of the announcement

* Documentation

### Moderation
[Not yet implemented]

### System
* Settings
  * `!tc settings` - Display current setting values

* Set
  * `!tc set <key> <value>` - Set value for bot settings

## Handled Events
* Member join:  Welcome message
  * Welcome message display format:
  ```
  Title here
  Content content content content content
  ```
  > **NOTICE**:  First line of the message will be displayed as title

  * Available placeholders:
    * `{user}` - Mentions the user
    * `{join_number}` - The orfinal number of user joined before this user (retrieved from server invite usage)(also count those left the server)
    * `{member_count}` - The number of member currently in the server (not counting those left the server)