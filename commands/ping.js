const { Command } = require("discord-akairo");

class PingCommand extends Command {
  constructor() {
    super("ping", {
      aliases: ["ping"],
      args: [{ id: "uselessArgs", match: "content" }],
      category: "util"
    });
  }

  exec(message, args) {
    let crap = args.uselessArgs;
    return message.util.reply("Pinging......").then(sent => {
      const timeDiff =
        (sent.editedAt || sent.createdAt) -
        (message.editedAt || message.createdAt);
      const text = `🔂\u2000**RTT**: ${timeDiff} ms\n💟\u2000**Heartbeat**: ${Math.round(
        this.client.ping
      )} ms`;
      return crap
        ? message.util.reply(
            `Pong! and here's the extra useless '${crap}' you entered\n${text}`
          )
        : message.util.reply(`Pong!\n${text}`);
    });
  }
}

module.exports = PingCommand;
