const { Command } = require("discord-akairo");
const DiscordJS = require("discord.js");
const prefix = require("config").get("prefix");
const util = require('../../util')

class SettingsCommand extends Command {
  constructor() {
    super("settings", {
      aliases: ["settings","setting"],
      ownerOnly: true,
      channelRestriction: "guild"
    });
  }

  exec(message) {
    let embed = new DiscordJS.RichEmbed();
    let announce_channel = this.client.settings.get(
      message.guild.id,
      "announce_channel"
    );
    let welcome_channel = this.client.settings.get(
      message.guild.id,
      "welcome_channel"
    );

    let notify_rank = this.client.settings.get(message.guild.id, "notify_rank");
    let welcome_message = this.client.settings.get(
      message.guild.id,
      "welcome_message"
    );
    let invite_link = this.client.settings.get(message.guild.id,"invite_link")
    // console.log(cleanTagsFormat(announce_channel));
    embed
      .setTitle(":wrench: Bot Configuration")
      .setThumbnail(this.client.user.avatarURL)
      .setFooter("© 2018 TrainCarts Development Team, Shelf ID: ZGVzdHJveWNjcGZvcmJldHRlcnRtcg==")
      .addField("Announce Channel", announce_channel ? `<#${util.cleanTagsFormat(announce_channel)}>` : `\`${prefix[1]}set announce_channel <channel_id>\``, true)
      .addField("Welcome Channel", welcome_channel ? `<#${util.cleanTagsFormat(welcome_channel)}>` : `\`${prefix[1]}set welcome_channel <channel_id>\``, true)
      .addField("Notify Rank", notify_rank ? `${this.client.util.resolveRole(util.cleanTagsFormat(notify_rank), message.guild.roles).name}` : `\`${prefix[1]}set notify_rank <role>\``, true)
      .addField("Invite Link", invite_link ? invite_link :`\`${prefix[1]}set invite_link <short_invite_link>\``,true)
      .addField("Welcome Message", welcome_message ? `${welcome_message}` : `\`${prefix[1]}set welcome_message <message>\``, false)
      .addBlankField();

    message.channel.send(embed);
  }
}

// Remove <#....>


module.exports = SettingsCommand;
