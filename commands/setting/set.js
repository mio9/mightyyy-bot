// Set bot guild-based settings
const { Command } = require("discord-akairo");
const settingKeys = require("../../enum/settingKeys");

class SetCommand extends Command {
  constructor() {
    super("set", {
      aliases: ["set"],
      ownerOnly: true,
      args: [
        { id: "settingKey", type: "string" },
        {
          id: "settingValue",
          match: 'rest'
        }
      ]
    });
  }

  exec(message, args) {
    if (!args.settingKey || !args.settingValue) {
      message.channel.send("❌Missing setting key or value");
      message.channel.send(help());
      return;
    } else {
      if (!settingKeys.includes(args.settingKey)) {
        message.channel.send("❌Your setting key is not valid");
        message.channel.send(help());
        return;
      }
      // message.channel.send(args.settingKey+" "+args.settingValue)
      this.client.settings
        .set(
          message.guild.id,
          args.settingKey,
          args.settingValue
        )
        .then(() => {
          return message.channel.send("✅ Bot settings updated");
        });
    }
  }
}

// function cleanTagsFormat(text) {
//   if (!text) {
//     return undefined;
//   }
//   return text.replace(/[\<\>\#\@\&]/g, "");
// }

function help() {
  return `Here is some setting keys: \`\`\`json\n ${JSON.stringify(
    settingKeys
  )}\`\`\``;
}

module.exports = SetCommand;
