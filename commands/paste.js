const { Command } = require("discord-akairo");
const request = require("request");
const DiscordJS = require("discord.js");

class PasteCommand extends Command {
  constructor() {
    super("paste", {
      aliases: ["paste", "haste"],
      args: [
        {
          id: "isID",
          match: "flag",
          prefix: "--id"
        },
        {
          id: "content",
          match: "rest",
          prompt: {
            start: [
              "You can now paste your long content. If it is too long, split it up into multiple message",
              "Type `stop` when you are done"
            ],
            cancel: "✅ Paste cancelled",
            timeout: "❌ Paste timeout",
            infinite: true
          }
        }
      ],
      category: "util"
    });
  }

  async exec(message, args) {
    // message.util.addMessage(message)
    message.util.send("Pasting content.....");
    // console.log(message.util.messages)
    // if nothing is input
    if (!args.content) {
      return message.util.send(
        "⛔ Paste failed: Air is not a valid content to paste"
      );
    }
    // if ID flag present (indicate paste content of a message with ID instead of typed content)
    if (args.isID) {
      //? paste content to hastebin  paste()
      let id = args.content;
      let retrievedMessage = await this.client.util.fetchMessage(
        message.channel,
        id
      );
      if (!retrievedMessage) {
        return message.util.send(
          `⛔ Paste failed: Message ID ${
            args.content
          } does not refer to any message`
        );
      }
      let contentArray = [retrievedMessage.content];
      paste(contentArray, message);
      // message.util.send(retrievedMessage.content);
      return;
    } else {
      // it is not retrieved by ID, it is message or message[]
      if (Array.isArray(args.content)) {
        paste(args.content, message);
        return;
      } else {
        let contentArray = [args.content];
        paste(contentArray, message);
        return;
      }
    }
  }
}

/**
 *
 *
 * @param {array} content
 * @param {message} message
 */
function paste(content, message) {

  let joinedContent = content.join("\n");
  request(
    {
      method: "POST",
      url: "https://hastebin.com/documents",
      headers: {
        "Content-Type": "application/json"
      },
      body: joinedContent
    },
    async function(err, res, body) {
      if (err){
        message.util.send("Something is going wrong with hastebin right now, please try again later")
      }
      let key = JSON.parse(res.toJSON().body).key;
      let preview = joinedContent
        .split(" ")
        .slice(0, 100)
        .join(" ");
      let embed = new DiscordJS.RichEmbed()
        .setDescription(`\nPreview\`\`\`${preview} ...\`\`\` `)
        .setTitle(`**https://hastebin.com/${key}**`)
        .setAuthor(
          message.author.username + " created a paste",
          message.author.displayAvatarURL
        )
        .setTimestamp();
      // do the thing

      console.log(key);
      // channel.send("Data pasted, check log");
      let junkMessages = await message.channel.fetchMessages({after: message.id})
      message.channel
        .bulkDelete(junkMessages)
        .then(deletedMsg => {
          deletedMsg.array().forEach(element => {
            console.log(element.id);
          });
          console.log("messages deleted");
          message.channel.send(embed);
        })
        .catch(console.error);

      // console.log(message.util.messages)
    }
  );
}

module.exports = PasteCommand;
