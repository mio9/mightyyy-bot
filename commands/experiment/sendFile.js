const { Command } = require("discord-akairo");
const fs = require('fs')

class ReadFileCommand extends Command {
  constructor() {
    super("send-file", {
      aliases: ["sendfile"],
      args:[
        {id: "filename"}
      ],
      ownerOnly: true
    });
  }

  async exec(message, args) {
    fs.readFile(args.filename,{encoding:'utf-8'},(err,data)=>{
      message.channel.send(data)
    })
  }
}

module.exports = ReadFileCommand;
