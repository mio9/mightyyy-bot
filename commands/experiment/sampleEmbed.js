const { Command } = require("discord-akairo");
const DiscordJS = require("discord.js");

class SampleEmbedCommand extends Command {
  constructor() {
    super("sample-embed", {
      aliases: ["sampleembed"]
    });
  }

  exec(message, args) {
    let embed = new DiscordJS.RichEmbed()
      .setAuthor("Author John")
      .setTitle("This is an example embed title")
      .setURL("https://www.example.com")
      .setDescription("This is description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et vulputate dolor, quis gravida arcu. Maecenas eu cursus ante, non tincidunt nunc. Morbi lobortis lectus nisi, in tempor augue malesuada volutpat.")
      .setFooter("This section is footer")
      .setTimestamp()
      .setThumbnail("http://i.beeimg.com/resources/tempimages/8872186a3573ebae8e2fe5e02d4559c4:2018484.png")
      .setColor("7289DA") //color of DISCORD
      .setImage("https://discordapp.com/assets/ba74954dde74ff40a32ff58069e78c36.png");
    message.channel.send(embed);
  }
}

module.exports = SampleEmbedCommand;
