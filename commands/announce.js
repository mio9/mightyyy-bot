const { Command } = require("discord-akairo");
const DiscordJS = require("discord.js");
const util = require("../util");

class AnnounceCommand extends Command {
  constructor() {
    super("announce", {
      aliases: ["announce", "a"],
      ownerOnly: true,
      channelRestriction: "guild",
      split: "sticky",
      args: [
        {
          id: "title",
          type: "string",
          match: "prefix",
          prefix: "--title=",
          default: "TrainCarts Server Announcement"
        },
        {
          id: "content",
          type: "string",
          match: "rest"
        },
        {
          id: "notify",
          match: "flag",
          prefix: "--notify"
        },
        {
          id: "color",
          match: "prefix",
          prefix: "--color=",
          default: util.getRandomColor()
        }
      ],
      category: "util"
    });
  }

  exec(message, args) {
    let content = args.content;
    const time = new Date();
    let announceChannelId = this.client.settings.get(
      message.guild.id,
      "announce_channel"
    );
    if (!announceChannelId) {
      return this.handler.emit(
        "commandFailed",
        message,
        this,
        "announce_channel_not_set"
      );
    }
    let notifyRankId = this.client.settings.get(
      message.guild.id,
      "notify_rank"
    );
    if (!notifyRankId) {
      return this.handler.emit(
        "commandFailed",
        message,
        this,
        "notify_rank_not_set"
      );
    }
    let announceChannel = this.client.util.resolveChannel(
      announceChannelId,
      message.guild.channels
    );
    if (args.notify) {
      content = `<@&${util.cleanTagsFormat(notifyRankId)}>\n` + content;
    }
    let announceMessage = new DiscordJS.RichEmbed()
      .setTitle(args.title)
      .setDescription(
        `${content} \n\nKind Regards,\n ${message.author.username}`
      )
      .setFooter(
        `© TC & BKCL Staff Team • ${time.getUTCDate()}/${time.getUTCMonth()}/${time.getUTCFullYear()} ${time.getUTCHours()}:${time.getUTCMinutes()} (UTC)`
      )
      .setColor(args.color)
      .setThumbnail(message.author.displayAvatarURL);
    // console.log(announceChannelId)

    announceChannel.send(announceMessage).then(
      sentmsg =>
        // message.delete().then(delmsg => {
        message.channel.send(
          `✅ Announcement sent with id: ${
            sentmsg.id
          } in channel <#${util.cleanTagsFormat(announceChannelId)}>`
        )
      // })
    );
    // console.log(JSON.stringify(args));
    // console.log(content);
  }
}

module.exports = AnnounceCommand;
