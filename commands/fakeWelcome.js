const { Command } = require("discord-akairo");

class FakeWelcomeCommand extends Command {
  constructor() {
    super("fake-welcome", {
      aliases: ["fakewelcome", "fw"],
      ownerOnly: true,
      args: [{id: "member", type:'member'}],
      category: "util"
    });
  }

  exec(message, args) {
    this.client.emit("guildMemberAdd",args.member);
    message.channel.send("✅ Manual member join event emitted")
  }
}

module.exports = FakeWelcomeCommand;
