const { Command } = require("discord-akairo");

//todo use TCBot manage rank instead if discord permission
class ClearCommand extends Command {
  constructor() {
    super("clear", {
      aliases: ["clear"],
      args: [{ id: "value", type: 'integer'}],
      channelRestriction: 'guild',
      userPermissions: ['MANAGE_MESSAGES'],
      category: "mod"
    });
  }

  async exec(message, args) {
    if (args.value>200){
      return message.channel.send("⛔ Deleting more than 200 messages at a time will take up a considerable amount of time, try a number smaller than 200")
    }
    message.channel
      .bulkDelete(args.value)
      .then(deletedMsg => {
        deletedMsg.array().forEach(element => {
          console.log(element.id);
        });
        console.log("messages deleted");
        message.channel.send(`✅ Cleared last ${args.value} messages`);
      })
      .catch(console.error);
  }
}

module.exports = ClearCommand;
