const { Command } = require("discord-akairo");

//todo use TCBot manage rank instead if discord permission
class ClearAfterCommand extends Command {
  constructor() {
    super("clear-after", {
      aliases: ["clearafter"],
      args: [{ id: "anchorMessage", type: "integer" }],
      channelRestriction:'guild',
      userPermissions:['MANAGE_MESSAGES'],
      category: "mod"
    });
  }

  async exec(message, args) {
    if (!args.anchorMessage) {
      return message.channel.send(
        "⛔ You need to input message ID for this command to work"
      );
    }
    try {
      let junkMessages = await message.channel.fetchMessages({
        after: args.anchorMessage
      });
      message.channel
        .bulkDelete(junkMessages)
        .then(deletedMsg => {
          deletedMsg.array().forEach(element => {
            console.log(element.id);
          });
          console.log("messages deleted");
          message.channel.send(`✅ Cleared last ${deletedMsg.array().length} messages`);
        })
        .catch(console.error);
    } catch (err) {
      console.error(err);
      return message.channel.send(
        "⛔ Command cannot be executed: Message not found or other error"
      );
    }
    
  }
}

module.exports = ClearAfterCommand;
