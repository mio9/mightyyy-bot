const { Command } = require("discord-akairo");
const DiscordJS = require("discord.js");

class IssueCommand extends Command {
  constructor() {
    super("issues", {
      aliases: ["issues", "bugs", "report", "issue"]
    });
  }

  exec(message, args) {
    let embed = new DiscordJS.RichEmbed()
      .setTitle("TC/BKCL Issue Reporting")
      .setDescription("If you have problem using the plugin, try discuss about it in plugin discussion channels first. Create new issues only when you think the problem is serious or may not be able to cover in short time")
      // avatar of berger on github
      .setThumbnail("https://avatars0.githubusercontent.com/u/11248334?s=200&v=4")
      .addField("TC / BKCL", "[Traincarts](https://github.com/bergerhealer/TrainCarts/issues) - Create an issue for traincarts\n\
        [BKCommonLib](https://github.com/bergerhealer/BKCommonLib/issues) - Create an issue for BKCommonLib\n\
        [TC-Coaster](https://github.com/bergerhealer/TC-Coasters/issues) - Create issue for TC Coaster (an addon to TrainCarts)")
      .addField("TC Bot (this bot)", "https://gitlab.com/traincarts/tc-bot/issues")
      .setFooter("© 2018 TrainCarts Development Team");
    message.channel.send(embed);
  }
}

module.exports = IssueCommand;
