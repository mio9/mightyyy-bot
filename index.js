const config = require("config");
const sqlite = require("sqlite");
const { AkairoClient, SQLiteProvider } = require("discord-akairo");

class TCBot extends AkairoClient {
  constructor() {
    super(
      {
        ownerID: config.get("owners"),
        prefix: config.get("prefix"),
        handleEdits: true,
        commandUtil: true,
        allowMention: true,
        commandDirectory: "./commands/",
        inhibitorDirectory: "./inhibitors/",
        listenerDirectory: "./listeners/",
        emitters: {
          process
        }
      },
      { disableEveryone: true }
    );

    this.settings = new SQLiteProvider(
      sqlite.open("./data/db.sqlite"),
      "guild_settings",
      {
        idColumn: "guild_id",
        dataColumn: "settings"
      }
    );
  }

  login(token) {
    return this.settings.init().then(() => super.login(token));
  }
}

const bot = new TCBot();
bot.login(config.get("token"));

// function displayHelp() {
//   console.log("w");
// }
